// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AsInventory/Public/AAsInventory_Game_Mode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAAsInventory_Game_Mode() {}

// Begin Cross Module References
ASINVENTORY_API UClass* Z_Construct_UClass_AAsInventory_Game_Mode();
ASINVENTORY_API UClass* Z_Construct_UClass_AAsInventory_Game_Mode_NoRegister();
ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
UPackage* Z_Construct_UPackage__Script_AsInventory();
// End Cross Module References

// Begin Class AAsInventory_Game_Mode
void AAsInventory_Game_Mode::StaticRegisterNativesAAsInventory_Game_Mode()
{
}
IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAsInventory_Game_Mode);
UClass* Z_Construct_UClass_AAsInventory_Game_Mode_NoRegister()
{
	return AAsInventory_Game_Mode::StaticClass();
}
struct Z_Construct_UClass_AAsInventory_Game_Mode_Statics
{
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "// AAsInventoryGameMode\n" },
#endif
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "AAsInventory_Game_Mode.h" },
		{ "ModuleRelativePath", "Public/AAsInventory_Game_Mode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "AAsInventoryGameMode" },
#endif
	};
#endif // WITH_METADATA
	static UObject* (*const DependentSingletons[])();
	static constexpr FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAsInventory_Game_Mode>::IsAbstract,
	};
	static const UECodeGen_Private::FClassParams ClassParams;
};
UObject* (*const Z_Construct_UClass_AAsInventory_Game_Mode_Statics::DependentSingletons[])() = {
	(UObject* (*)())Z_Construct_UClass_AGameModeBase,
	(UObject* (*)())Z_Construct_UPackage__Script_AsInventory,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Game_Mode_Statics::DependentSingletons) < 16);
const UECodeGen_Private::FClassParams Z_Construct_UClass_AAsInventory_Game_Mode_Statics::ClassParams = {
	&AAsInventory_Game_Mode::StaticClass,
	"Game",
	&StaticCppClassTypeInfo,
	DependentSingletons,
	nullptr,
	nullptr,
	nullptr,
	UE_ARRAY_COUNT(DependentSingletons),
	0,
	0,
	0,
	0x008802ACu,
	METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Game_Mode_Statics::Class_MetaDataParams), Z_Construct_UClass_AAsInventory_Game_Mode_Statics::Class_MetaDataParams)
};
UClass* Z_Construct_UClass_AAsInventory_Game_Mode()
{
	if (!Z_Registration_Info_UClass_AAsInventory_Game_Mode.OuterSingleton)
	{
		UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAsInventory_Game_Mode.OuterSingleton, Z_Construct_UClass_AAsInventory_Game_Mode_Statics::ClassParams);
	}
	return Z_Registration_Info_UClass_AAsInventory_Game_Mode.OuterSingleton;
}
template<> ASINVENTORY_API UClass* StaticClass<AAsInventory_Game_Mode>()
{
	return AAsInventory_Game_Mode::StaticClass();
}
DEFINE_VTABLE_PTR_HELPER_CTOR(AAsInventory_Game_Mode);
AAsInventory_Game_Mode::~AAsInventory_Game_Mode() {}
// End Class AAsInventory_Game_Mode

// Begin Registration
struct Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_Statics
{
	static constexpr FClassRegisterCompiledInInfo ClassInfo[] = {
		{ Z_Construct_UClass_AAsInventory_Game_Mode, AAsInventory_Game_Mode::StaticClass, TEXT("AAsInventory_Game_Mode"), &Z_Registration_Info_UClass_AAsInventory_Game_Mode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAsInventory_Game_Mode), 358061172U) },
	};
};
static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_612997627(TEXT("/Script/AsInventory"),
	Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_Statics::ClassInfo),
	nullptr, 0,
	nullptr, 0);
// End Registration
PRAGMA_ENABLE_DEPRECATION_WARNINGS
