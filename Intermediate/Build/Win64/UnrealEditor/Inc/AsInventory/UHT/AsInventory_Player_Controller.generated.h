// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "AsInventory_Player_Controller.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASINVENTORY_AsInventory_Player_Controller_generated_h
#error "AsInventory_Player_Controller.generated.h already included, missing '#pragma once' in AsInventory_Player_Controller.h"
#endif
#define ASINVENTORY_AsInventory_Player_Controller_generated_h

#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUI_Inventory(); \
	friend struct Z_Construct_UClass_UUI_Inventory_Statics; \
public: \
	DECLARE_CLASS(UUI_Inventory, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AsInventory"), NO_API) \
	DECLARE_SERIALIZER(UUI_Inventory)


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUI_Inventory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UUI_Inventory(UUI_Inventory&&); \
	UUI_Inventory(const UUI_Inventory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUI_Inventory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUI_Inventory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUI_Inventory) \
	NO_API virtual ~UUI_Inventory();


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_13_PROLOG
#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_16_INCLASS_NO_PURE_DECLS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASINVENTORY_API UClass* StaticClass<class UUI_Inventory>();

#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	DECLARE_FUNCTION(execCall_Widget_Event);


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_CALLBACK_WRAPPERS
#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUI_Inventory_Slot(); \
	friend struct Z_Construct_UClass_UUI_Inventory_Slot_Statics; \
public: \
	DECLARE_CLASS(UUI_Inventory_Slot, UUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/AsInventory"), NO_API) \
	DECLARE_SERIALIZER(UUI_Inventory_Slot)


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUI_Inventory_Slot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UUI_Inventory_Slot(UUI_Inventory_Slot&&); \
	UUI_Inventory_Slot(const UUI_Inventory_Slot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUI_Inventory_Slot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUI_Inventory_Slot); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUI_Inventory_Slot) \
	NO_API virtual ~UUI_Inventory_Slot();


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_32_PROLOG
#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_CALLBACK_WRAPPERS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_INCLASS_NO_PURE_DECLS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASINVENTORY_API UClass* StaticClass<class UUI_Inventory_Slot>();

#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAsInventory_Player_Controller(); \
	friend struct Z_Construct_UClass_AAsInventory_Player_Controller_Statics; \
public: \
	DECLARE_CLASS(AAsInventory_Player_Controller, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AsInventory"), NO_API) \
	DECLARE_SERIALIZER(AAsInventory_Player_Controller)


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_61_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AAsInventory_Player_Controller(AAsInventory_Player_Controller&&); \
	AAsInventory_Player_Controller(const AAsInventory_Player_Controller&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAsInventory_Player_Controller); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAsInventory_Player_Controller); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAsInventory_Player_Controller) \
	NO_API virtual ~AAsInventory_Player_Controller();


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_58_PROLOG
#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_61_INCLASS_NO_PURE_DECLS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASINVENTORY_API UClass* StaticClass<class AAsInventory_Player_Controller>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
