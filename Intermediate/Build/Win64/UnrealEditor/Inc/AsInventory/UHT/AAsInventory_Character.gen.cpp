// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AsInventory/Public/AAsInventory_Character.h"
#include "Runtime/Engine/Classes/Engine/HitResult.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAAsInventory_Character() {}

// Begin Cross Module References
ASINVENTORY_API UClass* Z_Construct_UClass_AAsInventory_Character();
ASINVENTORY_API UClass* Z_Construct_UClass_AAsInventory_Character_NoRegister();
ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
ENGINE_API UClass* Z_Construct_UClass_ACharacter();
ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
UPackage* Z_Construct_UPackage__Script_AsInventory();
// End Cross Module References

// Begin Class AAsInventory_Character Function IBox_Begin_End_Overlap
struct Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics
{
	struct AsInventory_Character_eventIBox_Begin_End_Overlap_Parms
	{
		UPrimitiveComponent* overlapped_comp;
		AActor* other_actor;
		UPrimitiveComponent* other_comp;
		int32 other_body_index;
	};
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AAsInventory_Character.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_overlapped_comp_MetaData[] = {
		{ "EditInline", "true" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_other_comp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FObjectPropertyParams NewProp_overlapped_comp;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_other_actor;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_other_comp;
	static const UECodeGen_Private::FIntPropertyParams NewProp_other_body_index;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static const UECodeGen_Private::FFunctionParams FuncParams;
};
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_overlapped_comp = { "overlapped_comp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_End_Overlap_Parms, overlapped_comp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_overlapped_comp_MetaData), NewProp_overlapped_comp_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_other_actor = { "other_actor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_End_Overlap_Parms, other_actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(0, nullptr) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_other_comp = { "other_comp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_End_Overlap_Parms, other_comp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_other_comp_MetaData), NewProp_other_comp_MetaData) };
const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_other_body_index = { "other_body_index", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_End_Overlap_Parms, other_body_index), METADATA_PARAMS(0, nullptr) };
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_overlapped_comp,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_other_actor,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_other_comp,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::NewProp_other_body_index,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::PropPointers) < 2048);
const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAsInventory_Character, nullptr, "IBox_Begin_End_Overlap", nullptr, nullptr, Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::PropPointers), sizeof(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::AsInventory_Character_eventIBox_Begin_End_Overlap_Parms), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::Function_MetaDataParams), Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::Function_MetaDataParams) };
static_assert(sizeof(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::AsInventory_Character_eventIBox_Begin_End_Overlap_Parms) < MAX_uint16);
UFunction* Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap()
{
	static UFunction* ReturnFunction = nullptr;
	if (!ReturnFunction)
	{
		UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap_Statics::FuncParams);
	}
	return ReturnFunction;
}
DEFINE_FUNCTION(AAsInventory_Character::execIBox_Begin_End_Overlap)
{
	P_GET_OBJECT(UPrimitiveComponent,Z_Param_overlapped_comp);
	P_GET_OBJECT(AActor,Z_Param_other_actor);
	P_GET_OBJECT(UPrimitiveComponent,Z_Param_other_comp);
	P_GET_PROPERTY(FIntProperty,Z_Param_other_body_index);
	P_FINISH;
	P_NATIVE_BEGIN;
	P_THIS->IBox_Begin_End_Overlap(Z_Param_overlapped_comp,Z_Param_other_actor,Z_Param_other_comp,Z_Param_other_body_index);
	P_NATIVE_END;
}
// End Class AAsInventory_Character Function IBox_Begin_End_Overlap

// Begin Class AAsInventory_Character Function IBox_Begin_Overlap
struct Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics
{
	struct AsInventory_Character_eventIBox_Begin_Overlap_Parms
	{
		UPrimitiveComponent* overlapped_comp;
		AActor* other_actor;
		UPrimitiveComponent* other_comp;
		int32 other_body_index;
		bool bfrom_sweep;
		FHitResult sweep_result;
	};
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "// all actors in collision box\n" },
#endif
		{ "ModuleRelativePath", "Public/AAsInventory_Character.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "all actors in collision box" },
#endif
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_overlapped_comp_MetaData[] = {
		{ "EditInline", "true" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_other_comp_MetaData[] = {
		{ "EditInline", "true" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_sweep_result_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FObjectPropertyParams NewProp_overlapped_comp;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_other_actor;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_other_comp;
	static const UECodeGen_Private::FIntPropertyParams NewProp_other_body_index;
	static void NewProp_bfrom_sweep_SetBit(void* Obj);
	static const UECodeGen_Private::FBoolPropertyParams NewProp_bfrom_sweep;
	static const UECodeGen_Private::FStructPropertyParams NewProp_sweep_result;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static const UECodeGen_Private::FFunctionParams FuncParams;
};
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_overlapped_comp = { "overlapped_comp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_Overlap_Parms, overlapped_comp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_overlapped_comp_MetaData), NewProp_overlapped_comp_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_other_actor = { "other_actor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_Overlap_Parms, other_actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(0, nullptr) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_other_comp = { "other_comp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_Overlap_Parms, other_comp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_other_comp_MetaData), NewProp_other_comp_MetaData) };
const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_other_body_index = { "other_body_index", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_Overlap_Parms, other_body_index), METADATA_PARAMS(0, nullptr) };
void Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_bfrom_sweep_SetBit(void* Obj)
{
	((AsInventory_Character_eventIBox_Begin_Overlap_Parms*)Obj)->bfrom_sweep = 1;
}
const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_bfrom_sweep = { "bfrom_sweep", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, sizeof(bool), sizeof(AsInventory_Character_eventIBox_Begin_Overlap_Parms), &Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_bfrom_sweep_SetBit, METADATA_PARAMS(0, nullptr) };
const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_sweep_result = { "sweep_result", nullptr, (EPropertyFlags)0x0010008008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AsInventory_Character_eventIBox_Begin_Overlap_Parms, sweep_result), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_sweep_result_MetaData), NewProp_sweep_result_MetaData) }; // 4100991306
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_overlapped_comp,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_other_actor,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_other_comp,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_other_body_index,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_bfrom_sweep,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::NewProp_sweep_result,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::PropPointers) < 2048);
const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAsInventory_Character, nullptr, "IBox_Begin_Overlap", nullptr, nullptr, Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::PropPointers), sizeof(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::AsInventory_Character_eventIBox_Begin_Overlap_Parms), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00440401, 0, 0, METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::Function_MetaDataParams), Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::Function_MetaDataParams) };
static_assert(sizeof(Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::AsInventory_Character_eventIBox_Begin_Overlap_Parms) < MAX_uint16);
UFunction* Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap()
{
	static UFunction* ReturnFunction = nullptr;
	if (!ReturnFunction)
	{
		UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap_Statics::FuncParams);
	}
	return ReturnFunction;
}
DEFINE_FUNCTION(AAsInventory_Character::execIBox_Begin_Overlap)
{
	P_GET_OBJECT(UPrimitiveComponent,Z_Param_overlapped_comp);
	P_GET_OBJECT(AActor,Z_Param_other_actor);
	P_GET_OBJECT(UPrimitiveComponent,Z_Param_other_comp);
	P_GET_PROPERTY(FIntProperty,Z_Param_other_body_index);
	P_GET_UBOOL(Z_Param_bfrom_sweep);
	P_GET_STRUCT_REF(FHitResult,Z_Param_Out_sweep_result);
	P_FINISH;
	P_NATIVE_BEGIN;
	P_THIS->IBox_Begin_Overlap(Z_Param_overlapped_comp,Z_Param_other_actor,Z_Param_other_comp,Z_Param_other_body_index,Z_Param_bfrom_sweep,Z_Param_Out_sweep_result);
	P_NATIVE_END;
}
// End Class AAsInventory_Character Function IBox_Begin_Overlap

// Begin Class AAsInventory_Character
void AAsInventory_Character::StaticRegisterNativesAAsInventory_Character()
{
	UClass* Class = AAsInventory_Character::StaticClass();
	static const FNameNativePtrPair Funcs[] = {
		{ "IBox_Begin_End_Overlap", &AAsInventory_Character::execIBox_Begin_End_Overlap },
		{ "IBox_Begin_Overlap", &AAsInventory_Character::execIBox_Begin_Overlap },
	};
	FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
}
IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAsInventory_Character);
UClass* Z_Construct_UClass_AAsInventory_Character_NoRegister()
{
	return AAsInventory_Character::StaticClass();
}
struct Z_Construct_UClass_AAsInventory_Character_Statics
{
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//-----------------------------------------------------------------------------------------------------------\n" },
#endif
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "AAsInventory_Character.h" },
		{ "ModuleRelativePath", "Public/AAsInventory_Character.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Interactive_Box_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/AAsInventory_Character.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Camera_Boom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/AAsInventory_Character.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Camera_Follow_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/AAsInventory_Character.h" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Interactive_Box;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Camera_Boom;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Camera_Follow;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static UObject* (*const DependentSingletons[])();
	static constexpr FClassFunctionLinkInfo FuncInfo[] = {
		{ &Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_End_Overlap, "IBox_Begin_End_Overlap" }, // 4278518654
		{ &Z_Construct_UFunction_AAsInventory_Character_IBox_Begin_Overlap, "IBox_Begin_Overlap" }, // 2255622779
	};
	static_assert(UE_ARRAY_COUNT(FuncInfo) < 2048);
	static constexpr FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAsInventory_Character>::IsAbstract,
	};
	static const UECodeGen_Private::FClassParams ClassParams;
};
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Character_Statics::NewProp_Interactive_Box = { "Interactive_Box", nullptr, (EPropertyFlags)0x01440000000a001d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Character, Interactive_Box), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Interactive_Box_MetaData), NewProp_Interactive_Box_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Character_Statics::NewProp_Camera_Boom = { "Camera_Boom", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Character, Camera_Boom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Camera_Boom_MetaData), NewProp_Camera_Boom_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Character_Statics::NewProp_Camera_Follow = { "Camera_Follow", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Character, Camera_Follow), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Camera_Follow_MetaData), NewProp_Camera_Follow_MetaData) };
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAsInventory_Character_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Character_Statics::NewProp_Interactive_Box,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Character_Statics::NewProp_Camera_Boom,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Character_Statics::NewProp_Camera_Follow,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Character_Statics::PropPointers) < 2048);
UObject* (*const Z_Construct_UClass_AAsInventory_Character_Statics::DependentSingletons[])() = {
	(UObject* (*)())Z_Construct_UClass_ACharacter,
	(UObject* (*)())Z_Construct_UPackage__Script_AsInventory,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Character_Statics::DependentSingletons) < 16);
const UECodeGen_Private::FClassParams Z_Construct_UClass_AAsInventory_Character_Statics::ClassParams = {
	&AAsInventory_Character::StaticClass,
	"Game",
	&StaticCppClassTypeInfo,
	DependentSingletons,
	FuncInfo,
	Z_Construct_UClass_AAsInventory_Character_Statics::PropPointers,
	nullptr,
	UE_ARRAY_COUNT(DependentSingletons),
	UE_ARRAY_COUNT(FuncInfo),
	UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Character_Statics::PropPointers),
	0,
	0x008000A4u,
	METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Character_Statics::Class_MetaDataParams), Z_Construct_UClass_AAsInventory_Character_Statics::Class_MetaDataParams)
};
UClass* Z_Construct_UClass_AAsInventory_Character()
{
	if (!Z_Registration_Info_UClass_AAsInventory_Character.OuterSingleton)
	{
		UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAsInventory_Character.OuterSingleton, Z_Construct_UClass_AAsInventory_Character_Statics::ClassParams);
	}
	return Z_Registration_Info_UClass_AAsInventory_Character.OuterSingleton;
}
template<> ASINVENTORY_API UClass* StaticClass<AAsInventory_Character>()
{
	return AAsInventory_Character::StaticClass();
}
DEFINE_VTABLE_PTR_HELPER_CTOR(AAsInventory_Character);
AAsInventory_Character::~AAsInventory_Character() {}
// End Class AAsInventory_Character

// Begin Registration
struct Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_Statics
{
	static constexpr FClassRegisterCompiledInInfo ClassInfo[] = {
		{ Z_Construct_UClass_AAsInventory_Character, AAsInventory_Character::StaticClass, TEXT("AAsInventory_Character"), &Z_Registration_Info_UClass_AAsInventory_Character, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAsInventory_Character), 859988429U) },
	};
};
static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_2434595983(TEXT("/Script/AsInventory"),
	Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_Statics::ClassInfo),
	nullptr, 0,
	nullptr, 0);
// End Registration
PRAGMA_ENABLE_DEPRECATION_WARNINGS
