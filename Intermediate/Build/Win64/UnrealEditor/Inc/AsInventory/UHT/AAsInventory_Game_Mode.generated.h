// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "AAsInventory_Game_Mode.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASINVENTORY_AAsInventory_Game_Mode_generated_h
#error "AAsInventory_Game_Mode.generated.h already included, missing '#pragma once' in AAsInventory_Game_Mode.h"
#endif
#define ASINVENTORY_AAsInventory_Game_Mode_generated_h

#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAsInventory_Game_Mode(); \
	friend struct Z_Construct_UClass_AAsInventory_Game_Mode_Statics; \
public: \
	DECLARE_CLASS(AAsInventory_Game_Mode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AsInventory"), ASINVENTORY_API) \
	DECLARE_SERIALIZER(AAsInventory_Game_Mode)


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AAsInventory_Game_Mode(AAsInventory_Game_Mode&&); \
	AAsInventory_Game_Mode(const AAsInventory_Game_Mode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ASINVENTORY_API, AAsInventory_Game_Mode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAsInventory_Game_Mode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAsInventory_Game_Mode) \
	ASINVENTORY_API virtual ~AAsInventory_Game_Mode();


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_8_PROLOG
#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_11_INCLASS_NO_PURE_DECLS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASINVENTORY_API UClass* StaticClass<class AAsInventory_Game_Mode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Game_Mode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
