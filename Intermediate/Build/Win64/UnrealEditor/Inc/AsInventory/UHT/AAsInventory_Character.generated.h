// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "AAsInventory_Character.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FHitResult;
#ifdef ASINVENTORY_AAsInventory_Character_generated_h
#error "AAsInventory_Character.generated.h already included, missing '#pragma once' in AAsInventory_Character.h"
#endif
#define ASINVENTORY_AAsInventory_Character_generated_h

#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	DECLARE_FUNCTION(execIBox_Begin_End_Overlap); \
	DECLARE_FUNCTION(execIBox_Begin_Overlap);


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAsInventory_Character(); \
	friend struct Z_Construct_UClass_AAsInventory_Character_Statics; \
public: \
	DECLARE_CLASS(AAsInventory_Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AsInventory"), NO_API) \
	DECLARE_SERIALIZER(AAsInventory_Character)


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AAsInventory_Character(AAsInventory_Character&&); \
	AAsInventory_Character(const AAsInventory_Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAsInventory_Character); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAsInventory_Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAsInventory_Character) \
	NO_API virtual ~AAsInventory_Character();


#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_13_PROLOG
#define FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_16_INCLASS_NO_PURE_DECLS \
	FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASINVENTORY_API UClass* StaticClass<class AAsInventory_Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AAsInventory_Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
