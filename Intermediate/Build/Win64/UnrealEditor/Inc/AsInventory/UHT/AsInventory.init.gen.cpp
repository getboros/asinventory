// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAsInventory_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_AsInventory;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_AsInventory()
	{
		if (!Z_Registration_Info_UPackage__Script_AsInventory.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/AsInventory",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xD09299AB,
				0x90D100A7,
				METADATA_PARAMS(0, nullptr)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_AsInventory.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_AsInventory.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_AsInventory(Z_Construct_UPackage__Script_AsInventory, TEXT("/Script/AsInventory"), Z_Registration_Info_UPackage__Script_AsInventory, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xD09299AB, 0x90D100A7));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
