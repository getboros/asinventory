// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AsInventory/Public/AsInventory_Player_Controller.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAsInventory_Player_Controller() {}

// Begin Cross Module References
ASINVENTORY_API UClass* Z_Construct_UClass_AAsInventory_Player_Controller();
ASINVENTORY_API UClass* Z_Construct_UClass_AAsInventory_Player_Controller_NoRegister();
ASINVENTORY_API UClass* Z_Construct_UClass_UUI_Inventory();
ASINVENTORY_API UClass* Z_Construct_UClass_UUI_Inventory_NoRegister();
ASINVENTORY_API UClass* Z_Construct_UClass_UUI_Inventory_Slot();
ASINVENTORY_API UClass* Z_Construct_UClass_UUI_Inventory_Slot_NoRegister();
COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
ENGINE_API UClass* Z_Construct_UClass_APlayerController();
ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputAction_NoRegister();
ENHANCEDINPUT_API UClass* Z_Construct_UClass_UInputMappingContext_NoRegister();
UMG_API UClass* Z_Construct_UClass_UImage_NoRegister();
UMG_API UClass* Z_Construct_UClass_UUserWidget();
UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
UMG_API UClass* Z_Construct_UClass_UVerticalBox_NoRegister();
UPackage* Z_Construct_UPackage__Script_AsInventory();
// End Cross Module References

// Begin Class UUI_Inventory
void UUI_Inventory::StaticRegisterNativesUUI_Inventory()
{
}
IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UUI_Inventory);
UClass* Z_Construct_UClass_UUI_Inventory_NoRegister()
{
	return UUI_Inventory::StaticClass();
}
struct Z_Construct_UClass_UUI_Inventory_Statics
{
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//------------------------------------------------------------------------------------------------------------\n" },
#endif
		{ "IncludePath", "AsInventory_Player_Controller.h" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_VerticalBox_Container_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FObjectPropertyParams NewProp_VerticalBox_Container;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static UObject* (*const DependentSingletons[])();
	static constexpr FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUI_Inventory>::IsAbstract,
	};
	static const UECodeGen_Private::FClassParams ClassParams;
};
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Inventory_Statics::NewProp_VerticalBox_Container = { "VerticalBox_Container", nullptr, (EPropertyFlags)0x0010000000080008, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(UUI_Inventory, VerticalBox_Container), Z_Construct_UClass_UVerticalBox_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_VerticalBox_Container_MetaData), NewProp_VerticalBox_Container_MetaData) };
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUI_Inventory_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Inventory_Statics::NewProp_VerticalBox_Container,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Statics::PropPointers) < 2048);
UObject* (*const Z_Construct_UClass_UUI_Inventory_Statics::DependentSingletons[])() = {
	(UObject* (*)())Z_Construct_UClass_UUserWidget,
	(UObject* (*)())Z_Construct_UPackage__Script_AsInventory,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Statics::DependentSingletons) < 16);
const UECodeGen_Private::FClassParams Z_Construct_UClass_UUI_Inventory_Statics::ClassParams = {
	&UUI_Inventory::StaticClass,
	nullptr,
	&StaticCppClassTypeInfo,
	DependentSingletons,
	nullptr,
	Z_Construct_UClass_UUI_Inventory_Statics::PropPointers,
	nullptr,
	UE_ARRAY_COUNT(DependentSingletons),
	0,
	UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Statics::PropPointers),
	0,
	0x00B010A0u,
	METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Statics::Class_MetaDataParams), Z_Construct_UClass_UUI_Inventory_Statics::Class_MetaDataParams)
};
UClass* Z_Construct_UClass_UUI_Inventory()
{
	if (!Z_Registration_Info_UClass_UUI_Inventory.OuterSingleton)
	{
		UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UUI_Inventory.OuterSingleton, Z_Construct_UClass_UUI_Inventory_Statics::ClassParams);
	}
	return Z_Registration_Info_UClass_UUI_Inventory.OuterSingleton;
}
template<> ASINVENTORY_API UClass* StaticClass<UUI_Inventory>()
{
	return UUI_Inventory::StaticClass();
}
UUI_Inventory::UUI_Inventory(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
DEFINE_VTABLE_PTR_HELPER_CTOR(UUI_Inventory);
UUI_Inventory::~UUI_Inventory() {}
// End Class UUI_Inventory

// Begin Class UUI_Inventory_Slot Function Call_Widget_Event
struct Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics
{
	struct UI_Inventory_Slot_eventCall_Widget_Event_Parms
	{
		double speed;
	};
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FDoublePropertyParams NewProp_speed;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static const UECodeGen_Private::FFunctionParams FuncParams;
};
const UECodeGen_Private::FDoublePropertyParams Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::NewProp_speed = { "speed", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(UI_Inventory_Slot_eventCall_Widget_Event_Parms, speed), METADATA_PARAMS(0, nullptr) };
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::NewProp_speed,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::PropPointers) < 2048);
const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Inventory_Slot, nullptr, "Call_Widget_Event", nullptr, nullptr, Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::PropPointers), sizeof(Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::UI_Inventory_Slot_eventCall_Widget_Event_Parms), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::Function_MetaDataParams), Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::Function_MetaDataParams) };
static_assert(sizeof(Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::UI_Inventory_Slot_eventCall_Widget_Event_Parms) < MAX_uint16);
UFunction* Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event()
{
	static UFunction* ReturnFunction = nullptr;
	if (!ReturnFunction)
	{
		UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event_Statics::FuncParams);
	}
	return ReturnFunction;
}
DEFINE_FUNCTION(UUI_Inventory_Slot::execCall_Widget_Event)
{
	P_GET_PROPERTY(FDoubleProperty,Z_Param_speed);
	P_FINISH;
	P_NATIVE_BEGIN;
	P_THIS->Call_Widget_Event(Z_Param_speed);
	P_NATIVE_END;
}
// End Class UUI_Inventory_Slot Function Call_Widget_Event

// Begin Class UUI_Inventory_Slot Function Widget_Event_In_BP
struct UI_Inventory_Slot_eventWidget_Event_In_BP_Parms
{
	double speed;
};
static FName NAME_UUI_Inventory_Slot_Widget_Event_In_BP = FName(TEXT("Widget_Event_In_BP"));
void UUI_Inventory_Slot::Widget_Event_In_BP(double speed)
{
	UI_Inventory_Slot_eventWidget_Event_In_BP_Parms Parms;
	Parms.speed=speed;
	ProcessEvent(FindFunctionChecked(NAME_UUI_Inventory_Slot_Widget_Event_In_BP),&Parms);
}
struct Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics
{
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
		{ "Category", "Vehicle" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FDoublePropertyParams NewProp_speed;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static const UECodeGen_Private::FFunctionParams FuncParams;
};
const UECodeGen_Private::FDoublePropertyParams Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::NewProp_speed = { "speed", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(UI_Inventory_Slot_eventWidget_Event_In_BP_Parms, speed), METADATA_PARAMS(0, nullptr) };
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::NewProp_speed,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::PropPointers) < 2048);
const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Inventory_Slot, nullptr, "Widget_Event_In_BP", nullptr, nullptr, Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::PropPointers), sizeof(UI_Inventory_Slot_eventWidget_Event_In_BP_Parms), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::Function_MetaDataParams), Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::Function_MetaDataParams) };
static_assert(sizeof(UI_Inventory_Slot_eventWidget_Event_In_BP_Parms) < MAX_uint16);
UFunction* Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP()
{
	static UFunction* ReturnFunction = nullptr;
	if (!ReturnFunction)
	{
		UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP_Statics::FuncParams);
	}
	return ReturnFunction;
}
// End Class UUI_Inventory_Slot Function Widget_Event_In_BP

// Begin Class UUI_Inventory_Slot
void UUI_Inventory_Slot::StaticRegisterNativesUUI_Inventory_Slot()
{
	UClass* Class = UUI_Inventory_Slot::StaticClass();
	static const FNameNativePtrPair Funcs[] = {
		{ "Call_Widget_Event", &UUI_Inventory_Slot::execCall_Widget_Event },
	};
	FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
}
IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UUI_Inventory_Slot);
UClass* Z_Construct_UClass_UUI_Inventory_Slot_NoRegister()
{
	return UUI_Inventory_Slot::StaticClass();
}
struct Z_Construct_UClass_UUI_Inventory_Slot_Statics
{
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//------------------------------------------------------------------------------------------------------------\n" },
#endif
		{ "IncludePath", "AsInventory_Player_Controller.h" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Image_Slot_0_MetaData[] = {
		{ "BindWidget", "" },
		{ "Category", "UI" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Image_Slot_0;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static UObject* (*const DependentSingletons[])();
	static constexpr FClassFunctionLinkInfo FuncInfo[] = {
		{ &Z_Construct_UFunction_UUI_Inventory_Slot_Call_Widget_Event, "Call_Widget_Event" }, // 3191860677
		{ &Z_Construct_UFunction_UUI_Inventory_Slot_Widget_Event_In_BP, "Widget_Event_In_BP" }, // 4023084919
	};
	static_assert(UE_ARRAY_COUNT(FuncInfo) < 2048);
	static constexpr FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUI_Inventory_Slot>::IsAbstract,
	};
	static const UECodeGen_Private::FClassParams ClassParams;
};
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Inventory_Slot_Statics::NewProp_Image_Slot_0 = { "Image_Slot_0", nullptr, (EPropertyFlags)0x002008000008001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(UUI_Inventory_Slot, Image_Slot_0), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Image_Slot_0_MetaData), NewProp_Image_Slot_0_MetaData) };
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUI_Inventory_Slot_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Inventory_Slot_Statics::NewProp_Image_Slot_0,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Slot_Statics::PropPointers) < 2048);
UObject* (*const Z_Construct_UClass_UUI_Inventory_Slot_Statics::DependentSingletons[])() = {
	(UObject* (*)())Z_Construct_UClass_UUserWidget,
	(UObject* (*)())Z_Construct_UPackage__Script_AsInventory,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Slot_Statics::DependentSingletons) < 16);
const UECodeGen_Private::FClassParams Z_Construct_UClass_UUI_Inventory_Slot_Statics::ClassParams = {
	&UUI_Inventory_Slot::StaticClass,
	nullptr,
	&StaticCppClassTypeInfo,
	DependentSingletons,
	FuncInfo,
	Z_Construct_UClass_UUI_Inventory_Slot_Statics::PropPointers,
	nullptr,
	UE_ARRAY_COUNT(DependentSingletons),
	UE_ARRAY_COUNT(FuncInfo),
	UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Slot_Statics::PropPointers),
	0,
	0x00B010A1u,
	METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Inventory_Slot_Statics::Class_MetaDataParams), Z_Construct_UClass_UUI_Inventory_Slot_Statics::Class_MetaDataParams)
};
UClass* Z_Construct_UClass_UUI_Inventory_Slot()
{
	if (!Z_Registration_Info_UClass_UUI_Inventory_Slot.OuterSingleton)
	{
		UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UUI_Inventory_Slot.OuterSingleton, Z_Construct_UClass_UUI_Inventory_Slot_Statics::ClassParams);
	}
	return Z_Registration_Info_UClass_UUI_Inventory_Slot.OuterSingleton;
}
template<> ASINVENTORY_API UClass* StaticClass<UUI_Inventory_Slot>()
{
	return UUI_Inventory_Slot::StaticClass();
}
UUI_Inventory_Slot::UUI_Inventory_Slot(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
DEFINE_VTABLE_PTR_HELPER_CTOR(UUI_Inventory_Slot);
UUI_Inventory_Slot::~UUI_Inventory_Slot() {}
// End Class UUI_Inventory_Slot

// Begin Class AAsInventory_Player_Controller
void AAsInventory_Player_Controller::StaticRegisterNativesAAsInventory_Player_Controller()
{
}
IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAsInventory_Player_Controller);
UClass* Z_Construct_UClass_AAsInventory_Player_Controller_NoRegister()
{
	return AAsInventory_Player_Controller::StaticClass();
}
struct Z_Construct_UClass_AAsInventory_Player_Controller_Statics
{
#if WITH_METADATA
	static constexpr UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//-------------------------------------------------------------------------------------------------------------\n" },
#endif
		{ "HideCategories", "Collision Rendering Transformation" },
		{ "IncludePath", "AsInventory_Player_Controller.h" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Active_Actor_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Interaction" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Inventory_Class_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "UI" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Inventory_Slot_Class_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "UI" },
#if !UE_BUILD_SHIPPING
		{ "Comment", "// Template Inventory\n" },
#endif
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "Template Inventory" },
#endif
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Default_Mapping_Context_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
#if !UE_BUILD_SHIPPING
		{ "Comment", "// Template Inventory Slots\n" },
#endif
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "Template Inventory Slots" },
#endif
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_LMB_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_RMB_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_MMB_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_Scroll_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
#if !UE_BUILD_SHIPPING
		{ "Comment", "// Used to Rotate Camera \n" },
#endif
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "Used to Rotate Camera" },
#endif
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_Jump_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_Move_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_Look_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_Interact_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
	static constexpr UECodeGen_Private::FMetaDataPairParam NewProp_Action_Inventory_Toggle_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/AsInventory_Player_Controller.h" },
	};
#endif // WITH_METADATA
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Active_Actor;
	static const UECodeGen_Private::FClassPropertyParams NewProp_Inventory_Class;
	static const UECodeGen_Private::FClassPropertyParams NewProp_Inventory_Slot_Class;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Default_Mapping_Context;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_LMB;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_RMB;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_MMB;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_Scroll;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_Jump;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_Move;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_Look;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_Interact;
	static const UECodeGen_Private::FObjectPropertyParams NewProp_Action_Inventory_Toggle;
	static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
	static UObject* (*const DependentSingletons[])();
	static constexpr FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAsInventory_Player_Controller>::IsAbstract,
	};
	static const UECodeGen_Private::FClassParams ClassParams;
};
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Active_Actor = { "Active_Actor", nullptr, (EPropertyFlags)0x0114000000020015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Active_Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Active_Actor_MetaData), NewProp_Active_Actor_MetaData) };
const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Inventory_Class = { "Inventory_Class", nullptr, (EPropertyFlags)0x0044000000010005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Inventory_Class), Z_Construct_UClass_UClass, Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Inventory_Class_MetaData), NewProp_Inventory_Class_MetaData) };
const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Inventory_Slot_Class = { "Inventory_Slot_Class", nullptr, (EPropertyFlags)0x0044000000010005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Inventory_Slot_Class), Z_Construct_UClass_UClass, Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Inventory_Slot_Class_MetaData), NewProp_Inventory_Slot_Class_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Default_Mapping_Context = { "Default_Mapping_Context", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Default_Mapping_Context), Z_Construct_UClass_UInputMappingContext_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Default_Mapping_Context_MetaData), NewProp_Default_Mapping_Context_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_LMB = { "Action_LMB", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_LMB), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_LMB_MetaData), NewProp_Action_LMB_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_RMB = { "Action_RMB", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_RMB), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_RMB_MetaData), NewProp_Action_RMB_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_MMB = { "Action_MMB", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_MMB), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_MMB_MetaData), NewProp_Action_MMB_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Scroll = { "Action_Scroll", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_Scroll), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_Scroll_MetaData), NewProp_Action_Scroll_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Jump = { "Action_Jump", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_Jump), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_Jump_MetaData), NewProp_Action_Jump_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Move = { "Action_Move", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_Move), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_Move_MetaData), NewProp_Action_Move_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Look = { "Action_Look", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_Look), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_Look_MetaData), NewProp_Action_Look_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Interact = { "Action_Interact", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_Interact), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_Interact_MetaData), NewProp_Action_Interact_MetaData) };
const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Inventory_Toggle = { "Action_Inventory_Toggle", nullptr, (EPropertyFlags)0x0040000000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(AAsInventory_Player_Controller, Action_Inventory_Toggle), Z_Construct_UClass_UInputAction_NoRegister, METADATA_PARAMS(UE_ARRAY_COUNT(NewProp_Action_Inventory_Toggle_MetaData), NewProp_Action_Inventory_Toggle_MetaData) };
const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAsInventory_Player_Controller_Statics::PropPointers[] = {
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Active_Actor,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Inventory_Class,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Inventory_Slot_Class,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Default_Mapping_Context,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_LMB,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_RMB,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_MMB,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Scroll,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Jump,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Move,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Look,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Interact,
	(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsInventory_Player_Controller_Statics::NewProp_Action_Inventory_Toggle,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Player_Controller_Statics::PropPointers) < 2048);
UObject* (*const Z_Construct_UClass_AAsInventory_Player_Controller_Statics::DependentSingletons[])() = {
	(UObject* (*)())Z_Construct_UClass_APlayerController,
	(UObject* (*)())Z_Construct_UPackage__Script_AsInventory,
};
static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Player_Controller_Statics::DependentSingletons) < 16);
const UECodeGen_Private::FClassParams Z_Construct_UClass_AAsInventory_Player_Controller_Statics::ClassParams = {
	&AAsInventory_Player_Controller::StaticClass,
	"Game",
	&StaticCppClassTypeInfo,
	DependentSingletons,
	nullptr,
	Z_Construct_UClass_AAsInventory_Player_Controller_Statics::PropPointers,
	nullptr,
	UE_ARRAY_COUNT(DependentSingletons),
	0,
	UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Player_Controller_Statics::PropPointers),
	0,
	0x009002A4u,
	METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_AAsInventory_Player_Controller_Statics::Class_MetaDataParams), Z_Construct_UClass_AAsInventory_Player_Controller_Statics::Class_MetaDataParams)
};
UClass* Z_Construct_UClass_AAsInventory_Player_Controller()
{
	if (!Z_Registration_Info_UClass_AAsInventory_Player_Controller.OuterSingleton)
	{
		UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAsInventory_Player_Controller.OuterSingleton, Z_Construct_UClass_AAsInventory_Player_Controller_Statics::ClassParams);
	}
	return Z_Registration_Info_UClass_AAsInventory_Player_Controller.OuterSingleton;
}
template<> ASINVENTORY_API UClass* StaticClass<AAsInventory_Player_Controller>()
{
	return AAsInventory_Player_Controller::StaticClass();
}
DEFINE_VTABLE_PTR_HELPER_CTOR(AAsInventory_Player_Controller);
AAsInventory_Player_Controller::~AAsInventory_Player_Controller() {}
// End Class AAsInventory_Player_Controller

// Begin Registration
struct Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_Statics
{
	static constexpr FClassRegisterCompiledInInfo ClassInfo[] = {
		{ Z_Construct_UClass_UUI_Inventory, UUI_Inventory::StaticClass, TEXT("UUI_Inventory"), &Z_Registration_Info_UClass_UUI_Inventory, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UUI_Inventory), 752153758U) },
		{ Z_Construct_UClass_UUI_Inventory_Slot, UUI_Inventory_Slot::StaticClass, TEXT("UUI_Inventory_Slot"), &Z_Registration_Info_UClass_UUI_Inventory_Slot, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UUI_Inventory_Slot), 1673137238U) },
		{ Z_Construct_UClass_AAsInventory_Player_Controller, AAsInventory_Player_Controller::StaticClass, TEXT("AAsInventory_Player_Controller"), &Z_Registration_Info_UClass_AAsInventory_Player_Controller, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAsInventory_Player_Controller), 525416654U) },
	};
};
static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_3535145572(TEXT("/Script/AsInventory"),
	Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Work_Unreal_Projects_AsInventory_Source_AsInventory_Public_AsInventory_Player_Controller_h_Statics::ClassInfo),
	nullptr, 0,
	nullptr, 0);
// End Registration
PRAGMA_ENABLE_DEPRECATION_WARNINGS
