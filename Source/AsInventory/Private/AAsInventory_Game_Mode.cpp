#include "AAsInventory_Game_Mode.h"

#include "AAsInventory_Character.h"
#include "AsInventory_Player_Controller.h"

// AAsInventory_Game_Mode
AAsInventory_Game_Mode::AAsInventory_Game_Mode()
{
	DefaultPawnClass = AAsInventory_Character::StaticClass();
	PlayerControllerClass = AAsInventory_Player_Controller::StaticClass();  // use our custom PlayerController class
}
//-----------------------------------------------------------------------------------------------------------
