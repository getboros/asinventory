#include "AsInventory_Item_Interactable.h"

#include "Kismet/GameplayStatics.h"  // UGameplayStatics
#include "Components/WidgetComponent.h"  // UWidgetComponent

// AAsInventory_Item_Interactable
AAsInventory_Item_Interactable::AAsInventory_Item_Interactable()
{
	PrimaryActorTick.bCanEverTick = false;

    Interactable_Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("InteractableMesh") );
    RootComponent = Interactable_Mesh;

    Widget_Interaction_Component = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetInteractionComponent") );
    Widget_Interaction_Component->SetupAttachment(Interactable_Mesh);

    Tags.Add("Item");
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Item_Interactable::Widget_Show()
{
    if (Widget_Interaction_Component->IsVisible() )
        Widget_Interaction_Component->SetVisibility(false);
    else
        Widget_Interaction_Component->SetVisibility(true);

    Interact();
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Item_Interactable::Interact_Implementation()
{
    int yy = 0;  // if event not called in BP we do those
}
//-----------------------------------------------------------------------------------------------------------
