#include "AsInventory_Player_Controller.h"

#include "AAsInventory_Character.h"

#include "Blueprint/UserWidget.h"
#include "Components/VerticalBox.h"
#include "Components/WidgetComponent.h"  // UWidgetComponent

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

// UUI_Inventory
void UUI_Inventory::Add_Slot(UUserWidget *slot)
{
	if (VerticalBox_Container && slot)
		VerticalBox_Container->AddChild(slot);
}
//-----------------------------------------------------------------------------------------------------------




// UInferior_UI
void UUI_Inventory_Slot::Call_Widget_Event(double speed)
{
	Widget_Event_In_BP(speed);  // call the Blueprint handler
}
//------------------------------------------------------------------------------------------------------------




// AAsInventory_Player_Controller
AAsInventory_Player_Controller::AAsInventory_Player_Controller()
 : Active_Actor(0), Is_Open(false), Inventory_Widget(0), Default_Mapping_Context(0), Action_LMB(0), Action_RMB(0), Action_MMB(0), Action_Scroll(0), Action_Jump(0), Action_Move(0),
   Action_Look(0), Action_Interact(0), Action_Inventory_Toggle(0)
{
	
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::BeginPlay()
{
	Super::BeginPlay();

	check(Inventory_Class);
	if (Inventory_Class != 0)
		Inventory_Widget = CreateWidget<UUI_Inventory>(GetWorld(), Inventory_Class);  // Template for inventory set in BP to Inventory_Class

	if (!Inventory_Widget != 0)
		return;
	
	Inventory_Widget->AddToViewport();
	Inventory_Widget->SetVisibility(ESlateVisibility::Collapsed);
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (UEnhancedInputLocalPlayerSubsystem* sub_system = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer() ) )
		sub_system->AddMappingContext(Default_Mapping_Context, 0);

	if (UEnhancedInputComponent *enhanced_input_component = Cast<UEnhancedInputComponent>(InputComponent))
	{
		enhanced_input_component->BindAction(Action_Jump, ETriggerEvent::Started, this, &AAsInventory_Player_Controller::Handle_Jump);
		enhanced_input_component->BindAction(Action_Jump, ETriggerEvent::Completed, this, &AAsInventory_Player_Controller::Handle_StopJumping);
		enhanced_input_component->BindAction(Action_Move, ETriggerEvent::Triggered, this, &AAsInventory_Player_Controller::Handle_Move);
		enhanced_input_component->BindAction(Action_Look, ETriggerEvent::Triggered, this, &AAsInventory_Player_Controller::Handle_Look);
		enhanced_input_component->BindAction(Action_Interact, ETriggerEvent::Started, this, &AAsInventory_Player_Controller::Handle_Interact);
		enhanced_input_component->BindAction(Action_Inventory_Toggle, ETriggerEvent::Started, this, &AAsInventory_Player_Controller::Handle_Inventory_Toggle);

		//enhanced_input_component->BindAction(Action_LMB, ETriggerEvent::Started, this, &AAsInventory_Player_Controller::Handle_LMB_Sarted);  // From Start
		//enhanced_input_component->BindAction(Action_LMB, ETriggerEvent::Triggered, this, &AAsInventory_Player_Controller::Handle_LMB_Triggered);  // Every Frame
		//enhanced_input_component->BindAction(Action_LMB, ETriggerEvent::Completed, this, &AAsInventory_Player_Controller::Handle_LMB_Released);  // To End
		//enhanced_input_component->BindAction(Action_LMB, ETriggerEvent::Canceled, this, &AAsInventory_Player_Controller::Handle_LMB_Released);  // When Released
		//enhanced_input_component->BindAction(Action_RMB, ETriggerEvent::Started, this, &AAsInventory_Player_Controller::Handle_RMB);
		//enhanced_input_component->BindAction(Action_MMB, ETriggerEvent::Started, this, &AAsInventory_Player_Controller::Handle_MMB);
		//enhanced_input_component->BindAction(Action_Scroll, ETriggerEvent::Triggered, this, &AAsInventory_Player_Controller::Handle_Scrolling);
	}
	else
		UE_LOG(LogTemp, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));

}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::Handle_Jump(const FInputActionValue& value)
{
	GetCharacter()->Jump();
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::Handle_StopJumping(const FInputActionValue& value)
{
	GetCharacter()->StopJumping();
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::Handle_Move(const FInputActionValue& value)
{
	const FVector2D movement_vector = value.Get<FVector2D>();

	const FRotator rotation = GetControlRotation();
	const FRotator yaw_rotation(0, rotation.Yaw, 0);  // find out which way is forward
	const FVector forward_direction = FRotationMatrix(yaw_rotation).GetUnitAxis(EAxis::X);  // get forward vector
	const FVector right_direction = FRotationMatrix(yaw_rotation).GetUnitAxis(EAxis::Y);  // get right vector 
	
	GetPawn()->AddMovementInput(forward_direction, movement_vector.Y);
	GetPawn()->AddMovementInput(right_direction, movement_vector.X);
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::Handle_Look(const FInputActionValue& value)
{
	const FVector2D look_axis_vector = value.Get<FVector2D>();

	AddYawInput(look_axis_vector.X);
	AddPitchInput(look_axis_vector.Y);
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::Handle_Interact(const FInputActionValue &value)
{
	bool is_hit;
	float trace_length;
	FVector trace_end;
	FVector camer_location;
	FRotator camera_rotation;
	FCollisionQueryParams trace_param(FName(TEXT("LineTrace")), true, this);
	FHitResult hit_result;
	UUI_Inventory_Slot *slot;

	// 1.2 Settings
	trace_length = 1000.0f;
	GetPlayerViewPoint(camer_location, camera_rotation);
	trace_end = camer_location + (camera_rotation.Vector() * trace_length);
	trace_param.bTraceComplex = false;
	trace_param.bReturnPhysicalMaterial = false;
	slot = 0;

	// 1.3 Draw debug || Use line trace
	DrawDebugLine(GetWorld(), camer_location, trace_end, FColor::Green, false, 1.0f, 0, 1.0f);
	is_hit = GetWorld()->LineTraceSingleByChannel(hit_result, camer_location, trace_end, ECC_Visibility, trace_param);

	// 1.4 If is hit get actor
	if (is_hit)
		if (hit_result.GetActor()->ActorHasTag(FName("Item") ) )
			Active_Actor = hit_result.GetActor();

	// 2.1 If have actor 
	if (!Active_Actor != 0)
		return;

	// 2.2 Add to inventory and destroy
	Inventory.Emplace(Active_Actor);
	Active_Actor->Destroy();  // Destroy object from world

	// 2.3 Add slot widget to Inventory widget
	if (!Inventory_Widget != 0)
		return;

	check(Inventory_Slot_Class);
	slot = CreateWidget<UUI_Inventory_Slot>(GetWorld(), Inventory_Slot_Class);
	if (slot)
		Inventory_Widget->Add_Slot(slot);
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Player_Controller::Handle_Inventory_Toggle(const FInputActionValue &value)
{
	if (Inventory_Widget && !Is_Open != false)  // !!! while press I open Inventory_Widget
		Inventory_Widget->SetVisibility(ESlateVisibility::Visible);
	else
		Inventory_Widget->SetVisibility(ESlateVisibility::Collapsed);
	
	Is_Open = !Is_Open;
}
//-----------------------------------------------------------------------------------------------------------
