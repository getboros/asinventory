#include "AAsInventory_Character.h"

#include "AsInventory_Item_Interactable.h"
#include "AsInventory_Player_Controller.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/BoxComponent.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// AAsInventory_Character 
AAsInventory_Character::AAsInventory_Character()
: Player_Controller(0), Actor_Interactive_Actor(0), Actor_Array{}, Interactive_Box(0), Camera_Boom(0), Camera_Follow(0)
{
	PrimaryActorTick.bCanEverTick = true;

	Interactive_Box = CreateDefaultSubobject<UBoxComponent>("Interactive Box");
	Interactive_Box->SetupAttachment(RootComponent);

	Interactive_Box->OnComponentBeginOverlap.AddDynamic(this, &AAsInventory_Character::IBox_Begin_Overlap);
	Interactive_Box->OnComponentEndOverlap.AddDynamic(this, &AAsInventory_Character::IBox_Begin_End_Overlap);
	Interactive_Box->bHiddenInGame = false;

	GetCapsuleComponent()->InitCapsuleSize(42.0f, 96.0f);  // Set size for collision capsule
		
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	GetCharacterMovement()->JumpZVelocity = 700.0f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.0f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.0f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.0f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	Camera_Boom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera_Boom") );
	Camera_Boom->SetupAttachment(RootComponent);
	Camera_Boom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	Camera_Boom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	Camera_Follow = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera") );
	Camera_Follow->SetupAttachment(Camera_Boom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	Camera_Follow->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Character::BeginPlay()
{
	Super::BeginPlay();

	Player_Controller = Cast<AAsInventory_Player_Controller>(GetController() );
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Character::Tick(float delta_time)
{
	Super::Tick(delta_time);  // !!!

	FString debug_message = FString::Printf(TEXT("Array_Overlap_Length %d"), Actor_Array.Num() );
	GEngine->AddOnScreenDebugMessage(1, 1.0f, FColor::Red, debug_message);
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Character::IBox_Begin_Overlap(UPrimitiveComponent *overlapped_comp, AActor *other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool bfrom_sweep, const FHitResult &sweep_result)
{
	if (!other_actor->ActorHasTag("Item") )
		return;
	else
		Actor_Array.AddUnique(other_actor);

	if (Actor_Interactive_Actor != 0)  // if two or more overlaped just enable 1 widget
		return;

	Player_Controller->Active_Actor = other_actor;

	Actor_Interactive_Actor = Cast<AAsInventory_Item_Interactable>(other_actor);  // cast to interactive actor || made abstract?
	if (Actor_Interactive_Actor != 0)
		Actor_Interactive_Actor->Widget_Show();
}
//-----------------------------------------------------------------------------------------------------------
void AAsInventory_Character::IBox_Begin_End_Overlap(UPrimitiveComponent *overlapped_comp, AActor *other_actor, UPrimitiveComponent* other_comp, int32 other_body_index)
{
	if (!Actor_Interactive_Actor != 0)
		return;

	Actor_Interactive_Actor->Widget_Show();  // Rename, interaction widget
	Actor_Array.Remove(Actor_Interactive_Actor);
	Actor_Interactive_Actor = 0;

	for (TObjectPtr<AActor> &next_actor : Actor_Array)
	{
		Actor_Interactive_Actor = Cast<AAsInventory_Item_Interactable>(next_actor);  // need set TSubClassOf?
		if (Actor_Interactive_Actor != 0)
			Actor_Interactive_Actor->Widget_Show();

		return;
	}
	Player_Controller->Active_Actor = 0;
}
//-----------------------------------------------------------------------------------------------------------
