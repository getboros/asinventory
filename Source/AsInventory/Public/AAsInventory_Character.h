#pragma once

#include "GameFramework/Character.h"
#include "AAsInventory_Character.generated.h"

// AAsInventory_Character 
class AAsInventory_Item_Interactable;
class AAsInventory_Player_Controller;
class UBoxComponent;
class USpringArmComponent;
class UCameraComponent;
//-----------------------------------------------------------------------------------------------------------
UCLASS(config=Game)
class AAsInventory_Character : public ACharacter
{
	GENERATED_BODY()

public:
	AAsInventory_Character();

	virtual void BeginPlay();
	virtual void Tick(float delta_time);


private:
	TObjectPtr<AAsInventory_Player_Controller> Player_Controller;
	TObjectPtr<AAsInventory_Item_Interactable> Actor_Interactive_Actor;  // first overlaped actor in box
	TArray<TObjectPtr<AActor> > Actor_Array;  // all actors in collision box

	UFUNCTION()
	void IBox_Begin_Overlap(UPrimitiveComponent *overlapped_comp, AActor *other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool bfrom_sweep, const FHitResult &sweep_result);

	UFUNCTION()
	void IBox_Begin_End_Overlap(UPrimitiveComponent *overlapped_comp, AActor *other_actor, UPrimitiveComponent *other_comp, int32 other_body_index);

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Default", meta = (AllowPrivateAccess = "true") )
	TObjectPtr<UBoxComponent> Interactive_Box;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true") )
	USpringArmComponent *Camera_Boom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true") )
	UCameraComponent *Camera_Follow;

};
//-----------------------------------------------------------------------------------------------------------
