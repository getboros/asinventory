#pragma once

#include "GameFramework/GameModeBase.h"
#include "AAsInventory_Game_Mode.generated.h"


// AAsInventoryGameMode
UCLASS(minimalapi)
class AAsInventory_Game_Mode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAsInventory_Game_Mode();

};
//-----------------------------------------------------------------------------------------------------------
