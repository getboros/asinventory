#pragma once

#include "AsInventory_Item_Interactable.generated.h"

// AAsInventory_Item_Interactable
class UWidgetComponent;
//-----------------------------------------------------------------------------------------------------------
UCLASS()
class ASINVENTORY_API AAsInventory_Item_Interactable : public AActor
{
	GENERATED_BODY()
	
public:	
	AAsInventory_Item_Interactable();

	void Widget_Show();

	UFUNCTION(BlueprintNativeEvent) void Interact();
	virtual void Interact_Implementation();

private:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Properties", meta = (AllowPrivateAccess = "true") )
	TObjectPtr<UWidgetComponent> Widget_Interaction_Component;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Properties", meta = (AllowPrivateAccess = "true") )
	TObjectPtr<UStaticMeshComponent> Interactable_Mesh;
};
//-----------------------------------------------------------------------------------------------------------
