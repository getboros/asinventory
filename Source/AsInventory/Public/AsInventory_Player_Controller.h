#pragma once

#include "Blueprint/UserWidget.h"  // !!! go with UInferior_UI

#include "GameFramework/PlayerController.h"
#include "AsInventory_Player_Controller.generated.h"


// UInferior_UI
class UVerticalBox;
class UUserWidget;
//------------------------------------------------------------------------------------------------------------
UCLASS()
class ASINVENTORY_API UUI_Inventory : public UUserWidget
{
	GENERATED_BODY()

public:
	void Add_Slot(UUserWidget *slot);
	
	UPROPERTY(meta = (BindWidget) )
	UVerticalBox *VerticalBox_Container;
};
//------------------------------------------------------------------------------------------------------------




// UUI_Inventory_Slot
class UImage;
//------------------------------------------------------------------------------------------------------------
UCLASS(abstract)
class ASINVENTORY_API UUI_Inventory_Slot : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION() void Call_Widget_Event(double speed);  // Called to update the speed display

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = Vehicle)
	void Widget_Event_In_BP(double speed);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UI", meta = (BindWidget) )
	UImage *Image_Slot_0;  // Bind in BP
};
//------------------------------------------------------------------------------------------------------------




// AAsInventory_Player_Controller
class AAsInventory_Item_Interactable;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;
//-------------------------------------------------------------------------------------------------------------
UCLASS()
class ASINVENTORY_API AAsInventory_Player_Controller : public APlayerController
{
	GENERATED_BODY()
	
public:
	AAsInventory_Player_Controller();

	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Interaction")
	TObjectPtr<AActor> Active_Actor;

private:
	void Handle_Move(const FInputActionValue &value);
	void Handle_Look(const FInputActionValue &value);
	void Handle_Interact(const FInputActionValue &value);  // when press E
	void Handle_Inventory_Toggle(const FInputActionValue &value);  // Pressed I
	void Handle_Jump(const FInputActionValue &value);
	void Handle_StopJumping(const FInputActionValue &value);

	bool Is_Open;
	TObjectPtr<UUI_Inventory> Inventory_Widget;
	TArray<AActor *> Inventory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "UI")
	TSubclassOf<UUserWidget> Inventory_Class;  // Template Inventory

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "UI")
	TSubclassOf<UUserWidget> Inventory_Slot_Class;  // Template Inventory Slots

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputMappingContext *Default_Mapping_Context;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_LMB;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_RMB;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_MMB;  // Used to Rotate Camera 
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_Scroll;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_Jump;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_Move;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_Look;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_Interact;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Input")
	UInputAction *Action_Inventory_Toggle;
};
//-----------------------------------------------------------------------------------------------------------
